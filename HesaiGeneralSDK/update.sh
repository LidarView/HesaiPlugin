#!/usr/bin/env bash

set -e
set -x
shopt -s dotglob

readonly name="hesaigeneralsdk"
readonly ownership="Hesai General Driver Upstream <kwrobot@kitware.com>"
readonly subtree="HesaiGeneralSDK/vtk$name"
readonly repo="https://gitlab.kitware.com/LidarView/third-party/hesai-general-driver.git"
readonly tag="for/lidarview-20231207-1.1.15"
readonly paths="
src/PandarGeneralRaw/

LICENSE

CMakeLists.vtk.txt
README.md
README.kitware.md
"

extract_source () {
    git_archive
    pushd "$extractdir/$name-reduced"

    # Remove unused files.
    rm -rvf src/PandarGeneralRaw/CMakeLists.txt
    rm -rvf src/PandarGeneralRaw/include/log.h
    rm -rvf src/PandarGeneralRaw/include/version.h
    rm -rvf src/PandarGeneralRaw/include/pandarGeneral/input.h
    rm -rvf src/PandarGeneralRaw/include/pandarGeneral/pandarGeneral.h
    rm -rvf src/PandarGeneralRaw/include/pandarGeneral/pcap_reader.h
    rm -rvf src/PandarGeneralRaw/src/input.cc
    rm -rvf src/PandarGeneralRaw/src/pandarGeneral.cc
    rm -rvf src/PandarGeneralRaw/src/pcap_reader.cpp

    mv -v CMakeLists.vtk.txt CMakeLists.txt
    popd
}

. "${BASH_SOURCE%/*}/../Utilities/update-common.sh"

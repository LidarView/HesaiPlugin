/*=========================================================================

  Program:   LidarView
  Module:    vtkHesaiSwiftDecoder.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkHesaiSwiftDecoder.h"
#include "vtkHesaiSwiftPacketFormat.h"

#include "laser_ts.h"

#include <vtkDelimitedTextReader.h>
#include <vtkMath.h>
#include <vtkNew.h>
#include <vtkObjectFactory.h>
#include <vtkTable.h>
#include <vtkVariant.h>

#include <bitset>
#include <cmath>

namespace
{
constexpr unsigned int MAX_PANDAR128_PACKET_SIZE = 900;

double degreeToRadian(double degree)
{
  return degree * vtkMath::Pi() / 180.0;
}
}

//-----------------------------------------------------------------------------
class vtkHesaiSwiftDecoder::vtkInternals
{
public:
  bool IsNewFrame(double newAzimuth)
  {
    double prevLastAzimuth = this->LastAzimuth;
    this->LastAzimuth = newAzimuth;
    return newAzimuth < prevLastAzimuth;
  }

public:
  std::vector<double> Cos_all_angle;
  std::vector<double> Sin_all_angle;
  hesai::swift::LasersTSOffset LaserOffset;

  std::vector<double> ElevationCorrection;
  std::vector<double> AzimuthCorrection;
  vtkNew<vtkTable> CalibrationData;

  size_t pointIdx = 0;
  double LastTimestamp = 0.0;
  double LastAzimuth = 0.0;
};

//-----------------------------------------------------------------------------
vtkHesaiSwiftDecoder::vtkHesaiSwiftDecoder(std::function<void(Point point)> fillPointsCallback,
  std::function<void(double timestamp)> splitFrameCallback)
  : Internals(new vtkHesaiSwiftDecoder::vtkInternals())
  , FillPointsCallback(fillPointsCallback)
  , SplitFrameCallback(splitFrameCallback)
{
  this->ResetCalibration();
}

//-----------------------------------------------------------------------------
vtkHesaiSwiftDecoder::~vtkHesaiSwiftDecoder() = default;

//-----------------------------------------------------------------------------
void vtkHesaiSwiftDecoder::ResetCalibration()
{
  auto& internals = this->Internals;

  // Initialize Elevation and Azimuth correction
  for (int i = 0; i < PANDAR128_LASER_NUM; i++)
  {
    if (elev_angle[i])
    {
      internals->ElevationCorrection.push_back(elev_angle[i]);
    }
    if (azimuth_offset[i])
    {
      internals->AzimuthCorrection.push_back(azimuth_offset[i]);
    }
  }

  for (int j = 0; j < PANDAR128_CIRCLE; j++)
  {
    double angle = j / 100.0;
    internals->Cos_all_angle.push_back(std::cos(degreeToRadian(angle)));
    internals->Sin_all_angle.push_back(std::sin(degreeToRadian(angle)));
  }
}

//-----------------------------------------------------------------------------
void vtkHesaiSwiftDecoder::LoadCorrectionFile(std::string filename)
{
  // Load the CSV file.
  // 2nd Column = Azimuth (Horizontal) Correction
  // 3rd Column = Elevation (Vertical) Correction

  vtkNew<vtkDelimitedTextReader> reader;
  reader->SetFileName(filename.c_str());
  reader->DetectNumericColumnsOn();
  reader->SetHaveHeaders(true);
  reader->SetFieldDelimiterCharacters(",");
  reader->SetStringDelimiter('"');
  reader->Update();

  // Extract the table.
  vtkTable* csvTable = reader->GetOutput();
  vtkIdType nRows = csvTable->GetNumberOfRows();

  auto& internals = this->Internals;
  internals->CalibrationData->ShallowCopy(csvTable);

  internals->ElevationCorrection.clear();
  internals->ElevationCorrection.resize(nRows);
  internals->AzimuthCorrection.clear();
  internals->AzimuthCorrection.resize(nRows);

  for (vtkIdType indexRow = 0; indexRow < nRows; ++indexRow)
  {
    double elevation = internals->CalibrationData->GetValue(indexRow, 1).ToDouble();
    double azimuth = internals->CalibrationData->GetValue(indexRow, 2).ToDouble();

    internals->ElevationCorrection[indexRow] = elevation;
    internals->AzimuthCorrection[indexRow] = azimuth;
  }
}

//-----------------------------------------------------------------------------
bool vtkHesaiSwiftDecoder::IsValidPacket(unsigned char const* vtkNotUsed(data),
  unsigned int dataLength)
{
  // The length of the data should match PANDAR128_PACKET_SIZE, but the tested data contains
  // more data. This variation could be cause by different Pandar128 UDP versions.
  // Currently, we accept a range of values to take in account this variability.
  bool validPacket =
    PANDAR128_PACKET_SIZE <= dataLength && dataLength < ::MAX_PANDAR128_PACKET_SIZE;
  return validPacket;
}

//-----------------------------------------------------------------------------
// https://github.com/HesaiTechnology/HesaiLidar_Pandar128_ROS/blob/160436018cbe4c96249c281499e2e9638d38b39c/pandar_pointcloud/src/conversions/convert.cc#L622
void vtkHesaiSwiftDecoder::ProcessLidarPacket(unsigned char const* data, unsigned int dataLength)
{
  if (!this->IsValidPacket(data, dataLength))
  {
    return;
  }
  auto& internals = this->Internals;

  const Pandar128Packet* dataPacket = reinterpret_cast<const Pandar128Packet*>(data);

  struct tm t;
  t.tm_year = dataPacket->tail.GetUTCTime0();
  t.tm_mon = dataPacket->tail.GetUTCTime1() - 1;
  t.tm_mday = dataPacket->tail.GetUTCTime2();
  t.tm_hour = dataPacket->tail.GetUTCTime3();
  t.tm_min = dataPacket->tail.GetUTCTime4();
  t.tm_sec = dataPacket->tail.GetUTCTime5();
  t.tm_isdst = 0;

  // Time in second of the packets
  time_t unix_second = (mktime(&t));

  int mode = dataPacket->tail.GetShutdownFlag() & 0x03;
  int state = (dataPacket->tail.GetShutdownFlag() & 0xF0) >> 4;
  int returnMode = dataPacket->tail.GetReturnMode();

  // Timestamp contains in the packet
  // roll back every second, probably in microsecond
  uint32_t timestampPacket = dataPacket->tail.GetTimestamp();

  // Timestamp in second of the packet
  internals->LastTimestamp = unix_second + (timestampPacket / 1000000.0);

  // [HACK start] Proccess only one return in case of dual mode for performance issue
  int start_block = 0;
  int end_block = PANDAR128_BLOCK_NUM;
  if (returnMode == 0x39 || returnMode == 0x3B || returnMode == 0x3C)
  {
    end_block = 1;
  }
  // [HACK end]

  for (int blockID = start_block; blockID < end_block; blockID++)
  {
    Pandar128Block currentBlock = dataPacket->blocks[blockID];

    if (internals->IsNewFrame(currentBlock.GetAzimuth()))
    {
      internals->pointIdx = 0;
      this->SplitFrameCallback(internals->LastTimestamp);
    }

    if (internals->pointIdx >= PANDAR128_POINT_NUM)
    {
      // SplitFrame for safety to not overflow allcoated arrays
      vtkGenericWarningMacro("Received more datapoints than expected");
      internals->pointIdx = 0;
      this->SplitFrameCallback(internals->LastTimestamp);
    }

    for (int laserID = 0; laserID < PANDAR128_LASER_NUM; laserID++)
    {

      double distance =
        static_cast<double>(currentBlock.units[laserID].GetDistance()) * PANDAR128_DISTANCE_UNIT;

      double azimuth_correction = internals->AzimuthCorrection[laserID];
      double elevation_correction = internals->ElevationCorrection[laserID];

      float azimuth = azimuth_correction + (static_cast<float>(currentBlock.GetAzimuth()) / 100.0f);
      float originAzimuth = azimuth;

      float pitch = elevation_correction;

      int offset = internals->LaserOffset.getTSOffset(
        laserID, mode, state, distance, dataPacket->header.GetVersionMajor());

      azimuth += internals->LaserOffset.getAngleOffset(
        offset, dataPacket->tail.GetMotorSpeed(), dataPacket->header.GetVersionMajor());

      pitch += internals->LaserOffset.getPitchOffset("", pitch, distance);

      if (pitch < 0)
      {
        pitch += 360.0f;
      }
      else if (pitch >= 360.0f)
      {
        pitch -= 360.0f;
      }

      float xyDistance = distance * internals->Cos_all_angle[static_cast<int>(pitch * 100 + 0.5)];
      azimuth += internals->LaserOffset.getAzimuthOffset("", originAzimuth, azimuth, xyDistance);

      int azimuthIdx = static_cast<int>(azimuth * 100 + 0.5);
      if (azimuthIdx >= CIRCLE)
      {
        azimuthIdx -= CIRCLE;
      }
      else if (azimuthIdx < 0)
      {
        azimuthIdx += CIRCLE;
      }

      // Skip wrong points
      if (distance < 0.1 || distance > 500.0)
        continue;

      Point point;
      point.x = xyDistance * internals->Sin_all_angle[azimuthIdx];
      point.y = xyDistance * internals->Cos_all_angle[azimuthIdx];
      point.z = distance * internals->Sin_all_angle[static_cast<int>(pitch * 100 + 0.5)];

      point.intensity = currentBlock.units[laserID].GetIntensity();
      point.ring = laserID;

      // Compute timestamp of the point
      point.timestamp = internals->LastTimestamp;
      point.timestamp +=
        internals->LaserOffset.getBlockTS(blockID, returnMode, mode, PANDAR128_LIDAR_NUM) /
          1000000000.0 +
        offset / 1000000000.0;

      this->FillPointsCallback(point);
      internals->pointIdx++;

      /*int index;
      if (LIDAR_RETURN_BLOCK_SIZE_2 == m_iReturnBlockSize)
      {
        index = (block.fAzimuth - start_angle_) / m_iAngleSize * PANDAR128_LASER_NUM *
                    m_iReturnBlockSize +
                PANDAR128_LASER_NUM * blockid + i;
        // ROS_WARN("block 2 index:[%d]",index);
      } else {
        index = (block.fAzimuth - start_angle_) / m_iAngleSize * PANDAR128_LASER_NUM + i;
      }
      cld->points[index] = point;*/
    }
  }
}

//-----------------------------------------------------------------------------
bool vtkHesaiSwiftDecoder::PreProcessLidarPacket(unsigned char const* data,
  unsigned int vtkNotUsed(dataLength))
{
  auto& internals = this->Internals;
  bool isNewFrame = false;

  const Pandar128Packet* dataPacket = reinterpret_cast<const Pandar128Packet*>(data);

  for (int blockID = 0; blockID < PANDAR128_BLOCK_NUM; blockID++)
  {
    Pandar128Block currentBlock = dataPacket->blocks[blockID];

    if (internals->IsNewFrame(currentBlock.GetAzimuth()))
    {
      isNewFrame = true;
    }
  }

  return isNewFrame;
}

//-----------------------------------------------------------------------------
double vtkHesaiSwiftDecoder::GetLastTimestamp()
{
  return this->Internals->LastTimestamp;
}

//-----------------------------------------------------------------------------
uint32_t vtkHesaiSwiftDecoder::GetMaxPointCloudNum()
{
  return PANDAR128_POINT_NUM;
}

//-----------------------------------------------------------------------------
uint32_t vtkHesaiSwiftDecoder::GetNumberOfPacketPoints()
{
  return PANDAR128_BLOCK_NUM * PANDAR128_LASER_NUM;
}

*************************************************************
**
** Revision history of HesaiSDK.
**
** Copyright (c) 2019, HesaiTechnology
**
** This source code is released under the Apache License.
** https://github.com/HesaiTechnology/HesaiLidar_General_SDK
**
*************************************************************

- Extract code from official repo https://github.com/HesaiTechnology/HesaiLidar_Swift_ROS
- Improve performances
- Remove warnings
- Add a namespace
